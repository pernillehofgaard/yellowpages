﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;

namespace Assignment3YellowPages
{
	class Program
	{
		static void Main(string[] args)
		{

			List<string> person = new List<string>();
			person.Add("NICK LENNOX");
			person.Add("DEWALS ELS");
			person.Add("PERNILLE HOFGAARD");
			person.Add("KARI NORDMANN");
			person.Add("OLA NORDMANN");

			
			Console.WriteLine("Search: ");
			string nameSearch = Console.ReadLine().ToUpper();

			foreach (string value in person.FindAll(element => element.Contains(nameSearch)))
			{
				Console.WriteLine(value.ToLower());
			}
			

			
		}
	}
}
